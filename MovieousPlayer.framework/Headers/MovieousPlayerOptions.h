//
//  MovieousPlayerOptions.h
//  MovieousPlayer
//
//  Created by Chris Wang on 2018/8/14.
//  Copyright © 2018 Movieous Team. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * @brief Decoder type
 */
typedef NS_ENUM(NSInteger, MovieousPlayerDecoderType) {
    /**
     * @brief Use Software decoder
     */
    MovieousPlayerDecoderTypeSoftware,
    /**
     * @brief Use Hardware decoder
     */
    MovieousPlayerDecoderTypeHardware,
};

@interface MovieousPlayerOptions : NSObject
    
/**
 * @brief Indicates whether the seeking request is accurate
 * The default is YES */
@property (nonatomic, assign) BOOL accurateSeeking;

/**
 * @brief Indicates that this is a live streaming video, so that the MovieousPlayer will try it's best to keep low delay to the source. What need to pay attention is that turn this property on can cause more lag.
 * The default is NO
 */
@property (nonatomic, assign) BOOL liveStreaming;

/**
 * @brief Indicates whether Movieous Player should try to catch up with the live source, you should only enable this option when you are playing live broadcast source
 * The default is NO
 */
@property (nonatomic, assign) BOOL delayOptimization;

/**
 * @brief Indicates that MovieousPlayer will be allowed to mix audio with other app
 * The default is YES
 */
@property (nonatomic, assign) BOOL allowMixAudioWithOthers;

/**
 * @brief allow local cache for video play
 * The default is YES
 */
@property (nonatomic, assign) BOOL localCache;

/**
 * @brief when localCache is on, player  can cache data from source in advance to file, this value speciifes the data capacity in bytes that player may cache data forward.
 * The default is 100 * 1024 * 1024(100MB)
 */
@property (nonatomic, assign) int cacheFileForwardsCapacity;

/**
 * @brief where to store cache files and map files when localCache is on.
 * The default is cache directory in user search domain.
 */
@property (nonatomic, strong) NSString *cacheFilesDirectory;

/**
 * @brief The type of decoder you want to use
 * The default is MovieousPlayerDecoderTypeHardware
 */
@property (nonatomic, assign) MovieousPlayerDecoderType decoderType;

/**
 * @brief Timeout interval in microsecond for network request proceeeded by MovieousPlayer
 * The default is 10 * 1000 * 1000 (10s)
 */
@property (nonatomic, assign) NSUInteger timeoutInterval;

/**
 * @brief Return the default option
 * @return The initialized default option
 */
+ (instancetype)defaultOptions;

@end

NS_ASSUME_NONNULL_END
