//
//  MovieousPlayerController.h
//  MovieousPlayer
//
//  Created by Chris Wang on 2018/8/14.
//  Copyright © 2018 Movieous Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MovieousPlayer/MovieousPlayerOptions.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * @brief 播放器内容的填充模式
 */
typedef NS_ENUM(NSInteger, MPScalingMode) {
    /**
     * @brief 保持比例填充到视图内，比例不一致会出现黑边
     */
    MPScalingModeAspectFit,
    /**
     * @brief 保持比例填充到视图内，比例不一致会发生裁剪
     */
    MPScalingModeAspectFill,
    /**
     * @brief 缩放后填充到视图内，比例不一致会出现变形
     */
    MPScalingModeFill
};

/**
 * @brief 播放器的状态
 */
typedef NS_ENUM(NSInteger, MPPlayerState) {
    /**
     * @brief 新初始化的播放器的状态
     */
    MPPlayerStateIdle,
    /**
     * @brief 播放器正在准备
     */
    MPPlayerStatePreparing,
    /**
     * @brief 播放器已经准备好播放
     */
    MPPlayerStatePrepared,
    /**
     * @brief 播放器正在播放
     */
    MPPlayerStatePlaying,
    /**
     * @brief 播放器因为网络问题正在缓冲
     */
    MPPlayerStateBuffering,
    /**
     * @brief 播放器正在查找位置进行播放
     */
    MPPlayerStateSeeking,
    /**
     * @brief 播放器已暂停
     */
    MPPlayerStatePaused,
    /**
     * @brief 媒体源已播放完成
     */
    MPPlayerStateCompleted,
    /**
     * @brief 播放器被停止
     */
    MPPlayerStateStopped,
    /**
     * @brief 播放器发生了一些错误
     */
    MPPlayerStateError,
};

/**
 * @brief 播放器停止的原因
 */
typedef NS_ENUM(NSInteger, MPFinishReason) {
    /**
     * @brief 由于播放停止播放停止
     */
    MPFinishReasonPlaybackEnded,
    /**
     * @brief 由于发生错误播放器停止
     */
    MPFinishReasonPlaybackError,
    /**
     * @brief 由于用户主动操作播放器停止
     */
    MPFinishReasonUserExited
};

/**
 * @brief 日志等级
 */
typedef NS_ENUM(NSInteger, MPLogLevel) {
    /**
     * @brief 未知日志等级
     */
    MPLogLevelUnknown,
    /**
     * @brief 打印 Default 及以上级别的日志
     */
    MPLogLevelDefault,
    /**
     * @brief 打印 Verbose 及以上级别的日志
     */
    MPLogLevelVerbose,
    /**
     * @brief 打印 Debug 及以上级别的日志
     */
    MPLogLevelDebug,
    /**
     * @brief 打印 Info 及以上级别的日志
     */
    MPLogLevelInfo,
    /**
     * @brief 打印 Warn 及以上级别的日志
     */
    MPLogLevelWarn,
    /**
     * @brief 打印 Error 及以上级别的日志
     */
    MPLogLevelError,
    /**
     * @brief 打印 Fatal 及以上级别的日志
     */
    MPLogLevelFatal,
    /**
     * @brief 打印 Silent 及以上级别的日志
     */
    MPLogLevelSilent
};

/**
 * @brief 播放器被其他音频打断时使用哪种策略
 */
typedef NS_ENUM(NSInteger, MPInterruptionOperation) {
    /**
     * @brief 暂停
     */
    MPInterruptionOperationPause,
    /**
     * @brief 停止
     */
    MPInterruptionOperationStop,
};

@class MovieousPlayerController;

@protocol MovieousPlayerControllerDelegate<NSObject>

@optional

/**
 * @brief 通知代理对象当前播放器已经成功渲染首帧视频
 * @param playerController 调用代理方法的播放器实例
 */
- (void)movieousPlayerControllerFirstVideoFrameRendered:(MovieousPlayerController *)playerController;

/**
 * @brief 通知代理对象当前播放器已经成功渲染首帧音频
 * @param playerController 调用代理方法的播放器实例
 */
- (void)movieousPlayerControllerFirstAudioFrameRendered:(MovieousPlayerController *)playerController;

/**
 * @brief 通知代理对象当前播放器状态已变更
 * @param playerController 调用代理方法的播放器实例
 * @param playState 新的播放器状态
 */
- (void)movieousPlayerController:(MovieousPlayerController *)playerController playStateDidChange:(MPPlayerState)playState;

/**
 * @brief 通知代理对象当前播放器状态已变更
 * @param playerController 调用代理方法的播放器实例
 * @param previousState 老的播放器状态
 * @param newState 新的播放器状态
 */
- (void)movieousPlayerController:(MovieousPlayerController *)playerController playStateDidChangeWithPreviousState:(MPPlayerState)previousState newState:(MPPlayerState)newState;

/**
 * @brief Notify delegate that MovieousPlayerController's has begun to retry reconnecting
 * @param playerController 调用代理方法的播放器实例
 * @param currentCount Current retried count
 */
- (BOOL)movieousPlayerController:(MovieousPlayerController *)playerController playerBeginsToRetry:(NSUInteger)currentCount;

/**
 * @brief Notify delegate that MovieousPlayerController has finished playing
 * @param playerController 调用代理方法的播放器实例
 * @param finishReason The reason why MovieousPlayerController has finished
 */
- (void)movieousPlayerController:(MovieousPlayerController *)playerController playFinished:(MPFinishReason)finishReason;

@end

@interface MovieousPlayerController : NSObject

@property (nonatomic, strong) NSURL *URL;

/**
 * @brief 全局的日志级别
 */
@property (nonatomic, assign, class) MPLogLevel logLevel;

/**
 * @brief 初始化播放器使用的 MovieousPlayerOptions 对象
 */
@property (nonatomic, strong, readonly) MovieousPlayerOptions *options;

/**
 * @brief 展示视频内容的视图
 */
@property (nonatomic, readonly) UIView *playerView;

/**
 * @brief 播放的视频总时长
 */
@property (nonatomic, readonly) NSTimeInterval totalDuration;

/**
 * @brief 播放器状态
 */
@property (nonatomic, readonly) MPPlayerState playState;

/**
 * @brief 视频的原始大小
 */
@property (nonatomic, readonly) CGSize naturalSize;

/**
 * @brief 视频的快照
 */
@property (nonatomic, readonly) UIImage *snapshot;

/**
 * @brief 当前播放到的时间进度
 */
@property (nonatomic, assign) NSTimeInterval currentTime;

 /**
 * @brief 当前可播放的时长，这部分数据已经被加载到了内存当中。
 */
@property (nonatomic, assign) NSTimeInterval playableDuration;

/**
 * @brief 媒体缓存区域的最大缓存空间大小。
 */
@property (nonatomic, assign) int maxBufferSize;

/**
 * @brief 当播放器内容和视图的比例不一致时如何填充内容
 * 默认为 MPScalingModeNone
 */
@property (nonatomic, assign) MPScalingMode scalingMode;

/**
 * @brief 播放器的音量
 * 默认为 1
 */
@property (nonatomic, assign) float volume;

/**
 * @brief 当应用进入后台时是否停止播放
 * 默认为 YES
 */
@property (nonatomic, assign) BOOL pauseInBackground DEPRECATED_ATTRIBUTE;

/**
 * @brief 当应用进入后台时是否会被打断，具体打断方式由 interruptionOperation 属性进行配置
 * 默认为 YES
 */
@property (nonatomic, assign) BOOL interruptInBackground;

/**
 * @brief 当应用进入后台后用什么操作打断当前播放
 * 默认为 MPInterruptionOperationPause
 */
@property (nonatomic, assign) MPInterruptionOperation interruptionOperation;

/**
 * @brief 如果发生错误是否重试
 * 默认为 YES
 */
@property (nonatomic, assign) BOOL retryIfError;

/**
 * @brief 播放器的代理对象
 */
@property (nonatomic, weak) id<MovieousPlayerControllerDelegate> delegate;

/**
 * @brief 播放结束后是否循环
 */
@property (nonatomic, assign) BOOL loop;

/**
 * @brief 使用媒体 URL 创建一个 MovieousPlayerController 实例。
 * @param URL 用于播放的媒体地址。
 * @return 初始化完成的播放器对象。
 */
+ (instancetype)playerControllerWithURL:(NSURL *)URL;

/**
 * @brief 使用媒体地址和配置选项来创建一个 MovieousPlayerController 实例
 * @param URL 用于播放的媒体地址
 * @param options 初始化播放器的选项
 * @return 初始化完成的播放器对象
 */
+ (instancetype)playerControllerWithURL:(NSURL *)URL options:(MovieousPlayerOptions *)options;

/**
 * @brief 使用媒体地址来初始化一个 MovieousPlayerController 实例
 * @param URL 用于播放的媒体地址
 * @return 初始化完成的播放器对象
 */
- (instancetype)initWithURL:(NSURL *)URL;

/**
 * @brief 使用媒体地址和配置选项来初始化一个 MovieousPlayerController 实例
 * @param URL 用于播放的媒体地址
 * @param options 初始化播放器的选项
 * @return 初始化完成的播放器对象
 */
- (instancetype)initWithURL:(NSURL *)URL options:(MovieousPlayerOptions *)options;

/**
 * @brief 准备播放
 */
- (void)prepareToPlay;

/**
 * @brief 开始播放
 */
- (void)play;

/**
 * @brief 暂停播放
 */
- (void)pause;

/**
 * @brief 停止播放
 */
- (void)stop;

@end

NS_ASSUME_NONNULL_END
