
//
//  MovieousPlayerOptions.h
//  MovieousPlayer
//
//  Created by Chris Wang on 2018/8/14.
//  Copyright © 2018 Movieous Team. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * @brief 解码器类型
 */
typedef NS_ENUM(NSInteger, MovieousPlayerDecoderType) {
    /**
     * @brief 使用软件解码器
     */
    MovieousPlayerDecoderTypeSoftware,
    /**
     * @brief 使用硬件解码器
     */
    MovieousPlayerDecoderTypeHardware,
};

@interface MovieousPlayerOptions : NSObject

/**
 * @brief 是否精确定位到请求的播放进度处。
 * 默认为 YES 
 */
@property (nonatomic, assign) BOOL accurateSeeking;

/**
 * @brief 标识是否是直播流播放，通过此标记 MovieousPlayer 会尽最大努力保证低延迟，但是需要注意的是打开此选项将会造成更多的卡顿。
 * 默认关闭
 */
@property (nonatomic, assign) BOOL liveStreaming;

/**
 * @brief 是否开启直播流的累积延迟优化机制，只有当前流式直播流时才应当能开启累积延迟优化。
 * 默认关闭
 */
@property (nonatomic, assign) BOOL delayOptimization;

/**
 * @brief 是否允许和其他音频发生混合而不被打断。
 * 默认开启
 */
@property (nonatomic, assign) BOOL allowMixAudioWithOthers;

/**
 * @brief 使用的解码器类型
 * 默认为 MovieousPlayerDecoderTypeHardware
 */
@property (nonatomic, assign) MovieousPlayerDecoderType decoderType;

/**
 * @brief 播放器网络请求的超时时间（以微秒为单位）。
 * 默认为 10 * 1000 * 1000 (10秒)
 */
@property (nonatomic, assign) NSUInteger timeoutInterval;

/**
 * @brief 创建一个默认的 MovieousPlayerOptions 对象
 * @return 返回默认的 MovieousPlayerOptions 对象
 */
+ (instancetype)defaultOptions;

@end

NS_ASSUME_NONNULL_END
