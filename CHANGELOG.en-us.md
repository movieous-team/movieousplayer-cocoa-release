*Read this in other languages: [English](CHANGELOG.en-us.md), [简体中文](CHANGELOG.md).*

# v1.0.8(2019-9-28)
## Fixes
- fix issue that playableTime may be error after play ended.
- fix issue that logLevel not applied on some components.

# v1.0.7(2019-7-24)
## Fixes
- fix live streaming add error.

# v1.0.6(2019-7-24)
## Fixes
- fix crash happened when play in some devices.

# v1.0.5(2019-7-17)
## Features
- add `accurateSeeking` property for accurate seeking.
## Fixes
- fix incorrect nullability for some classes.
- fix crash problem when resume after player stopped.

# v1.0.4(2019-5-28)
## Fixes
- fix disaccord between `currentPlaybackTime` and `duration` after player stopped.

# v1.0.3(2019-3-19)
## Features
- add `playableDuration` and `maxBufferSize`.
## Fixes
- Fix some bug.

# v1.0.2(2019-3-19)
## Fixes
- Temporally remove VR play function to avoid crash.
- fix some bugs while playing.

# v1.0.1(2018-12-8)
## Fixes
- Fix play problem after auto reconnect.

# v1.0.0(2018-11-28)
- Initial release.